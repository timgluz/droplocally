class Checkin
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :user, dependent: :delete

  scope :by_user, ->(user){ where(user_id: user.fid)}
  field :fid, type: String
  field :user_id, type: String
  field :type, type: String
  field :shout, type: String
  field :venue_id, type: String


  def venue
    Venue.where(fid: venue_id).shift
  end
end
