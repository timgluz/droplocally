class VenueController < ApplicationController

  def show
    fid = params[:fid]
    @user = current_user
    @checkin = Checkin.where(user_id: @user.id, fid: fid).shift
    @venue = Venue.where(fid: fid).shift 
    @folder_files = FileItem.by_parent_id(fid)
  end

  def autocomplete
    query = (params[:term] || params[:q])
    longitude = (params[:long] || 22.498)
    latitude = (params[:lat] || 69.4498)
    
    render json: [] if query.nil? or longitude.nil?

    api = Foursquare.new current_user
    results = api.suggest_venues query, latitude, longitude

    suggestions = []
    results['minivenues'].each do |venue|
      suggestions << {value: venue.id, label: venue.name}
    end
    render json: suggestions
  end

  def checkin
    api = Foursquare.new current_user
    user_coords = params[:ll]
    unless params[:user_latitude].nil?
      user_coords = "#{params[:user_latitude]},#{params[:user_longitude]}"
    end
    result = api.client.add_checkin venueId: params['venue_id'],
                                    broadcast: params['broadcast'],
                                    ll: user_coords,
                                    shout: params[:shout]
    p '------------------'
    p result
    
    venue = update_venue(result['venue'])
    checkin = add_checkin(result, venue, current_user)
    
    redirect_to action: 'show', fid: venue.fid
  end

  private
    def add_checkin(checkin, venue, user)
      new_checkin = Checkin.new fid: checkin['id'],
                                type: checkin['type'],
                                shout: checkin['shout'],
                                venue_id: venue[:fid],
                                user_id: user.fid
      new_checkin.save
      new_checkin 
    end
    def update_venue(venue)
      p '---------------'
      p venue

      venue_fields = {
        fid: venue['id'],
        name: venue['name'],
        contacts: venue['contact'],
        location: venue['location'],
        categories: venue['categories'],
        stats: venue['stats'],
        url: venue['url']
      }
      unless Venue.where(fid: venue_fields[:fid]).exists?
        new_venue = Venue.new(venue_fields)
        new_venue.save
        new_folder = FileItem.new fid: venue_fields[:id],
                                  s3_name: venue_fields[:name],
                                  original_name: venue_fields[:name],
                                  folder: true
  
        new_folder.save
      else
        new_venue = Venue.where(fid: venue_fields[:fid]).shift
        new_venue.update_attributes venue_fields
        p '--------------------'
        p 'updated existing venue'
        p new_venue['location']
      end
      
      return new_venue
    end
end
