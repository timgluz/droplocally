class Venue
  include Mongoid::Document
  
  field :fid, type: String #foursquare key
  field :name, type: String
  field :contacts, type: Hash
  field :location, type: Hash
  field :categories, type: Hash
  field :stats, type: Hash
  field :url, type: String

end
