class HomeController < ApplicationController
  include SessionsHelper

  def index
    @user = current_user
    @page = params[:page]
    @latest_checkins = Checkin.by_user(current_user).desc(:created_at).limit(10)
  end
end
