Droplocally::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Log error messages when you accidentally call methods on nil.
  config.whiny_nils = true

  # Show full error reports and disable caching
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send
  config.action_mailer.raise_delivery_errors = false

  # Print deprecation notices to the Rails logger
  config.active_support.deprecation = :log

  # Only use best-standards-support built into browsers
  config.action_dispatch.best_standards_support = :builtin


  # Do not compress assets
  config.assets.compress = false

  # Expands the lines which load the assets
  config.assets.debug = true

  AWS::S3::Base.establish_connection!(
    :access_key_id => "123",
    :secret_access_key => "abc",
    :server => "localhost",
    :port => "4567"
  )
  
  ENV['FSQR_CALLBACK'] = "http://127.0.0.1:3000/sessions/callback"
  ENV['FSQR_KEY'] = "KXRQW24LV5ZO1DRA2LI5JWHO2Z12WRQMWET3VB5BSB2XHFXC"
  ENV['FSQR_SECRET'] = "UNSVREZDGFMOXDS1PGNVYUEM3PQJF2P4X022SYYPQ3WQIXY1" 
end
