Droplocally::Application.routes.draw do
  get 'sessions/login', :to => 'sessions#login'
  get 'sessions/callback', :to => 'sessions#foursquare_callback'

  get 'home', :to => 'home#index'
  get 'venue/autocomplete', :to => 'venue#autocomplete'
  post 'venue/checkin', :to => 'venue#checkin'
  get 'venues/:fid', :to => 'venue#show'

  post 'venue/:fid/upload', :to => 'file_item#upload'
  get 'venues/:fid/download', :to => 'file_item#download'
  post 'venues/:fid/download', :to => 'file_item#download'
 root :to => 'welcome#index'
end
