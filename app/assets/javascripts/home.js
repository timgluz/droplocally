
$(function(){

  function render_checkin_form(ui){
    var extra_fields = [
      '<input name = "broadcast" type="hidden" value="public" />',
      '<input name = "shout" type="text" class="" placeholder ="Add your message" />',
      '<button type="submit" class="btn btn-primary">CheckIn</button>'
    ].join('');
    $('#checkin_form').append(extra_fields);
  }

  function init_venue_typeahead(){
    var user_lat = $('[name="user_latitude"]').val(),
        user_long = $('[name="user_longitude"]').val()
        uri = '/venue/autocomplete.json?lat='+user_lat + '&long=' + user_long;
  
    console.debug(uri);
    $('#venue_id').autocomplete({
      'source': uri,
      'minLength': 3,
      'select': function(event, ui){ 
        console.debug(ui);
        render_checkin_form(ui);
      }
    });
  }


  function set_user_location(position){
    console.log("lat:" + position.coords.latitude + "long:" + position.coords.longitude);
    $('[name="user_latitude"]').val(position.coords.latitude);
    $('[name="user_longitude"]').val(position.coords.longitude);
    $('input#venue_name').removeClass('disabled');
    $('#venue_search_btn').removeClass('disabled');
    $('#input#venue_id').val('');
    init_venue_typeahead();
  }

  console.debug("Reading user location:");
  if($("#checkin_form").length){
    var search_field = $('input[name="venue_id"]');
    if(navigator.geolocation){
      navigator.geolocation.getCurrentPosition(set_user_location);
    } else {
      search_field.val("Your browser dont support HTML5 geolocation.");
    }
  } else {
    console.log("skipping geolocating");
  }

  //active list view selection
   $('.checkin-item').on('click', function(e){
      e.preventDefault();
      jQuery.noConflict();
      $(this).toggleClass('selected');
      
      console.debug($(this).data());
      var venue = $(this).data();
      var check_form= ""
      if(venue){
        check_form = [
          '<img src="/assets/foursquare-logo.jpg" style="position:relative;left:20%; height:160px;"/>',
          '<br/>',
          '<input type="hidden" name="venue_id" value="' + venue.venue + '" /><br />',
          '<input type="hidden" name="ll" value="' + venue.ll + '" /><br />',
          '<input type="hidden" name="broadcast" value="' + venue.broadcast + '" /><br />',
          '<p> Add your message: </p>',
          '<textarea name="shout" rows="3" style="width:350px;" value="" /><br />',
          '<button type="submit"> Checkin and go to files </button>'
        ].join('');
        $("#checkin_modal .modal-body").html(check_form);
      }
      $("#checkin_modal").modal('toggle');

   });

  //active list view selection
   $('.listview-container').on('click', '.file-item', function(e){
      jQuery.noConflict();
      e.preventDefault();
      $(this).toggleClass('selected');

   });

   $('button.bulk_download').on('click', function(){
     var current_path = window.location.pathname; 
     var selected_files = []

      $(".file-item.selected").each(
        function(index, item){ 
          selected_files.push(
            $(item).data('s3_name')
          )
      });
      
      console.log("Going to read:");
      console.log(selected_files);
      selected_files = selected_files.join(",");
      window.location.href = current_path + "/download?s3_names=" + selected_files;
   });

   $('.win-command.delete').on('click', function(){
      alert("going o delete file?");
   });
});
