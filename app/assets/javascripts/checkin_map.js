$(function(){

dojo.require("esri.map");
dojo.require("esri.dijit.Popup");

var map;


function get_current_location(){
	var coords = null;

	//Get the latitude and the longitude;
	function success_function(position) {
	    var lat = position.coords.latitude;
	    var lng = position.coords.longitude;
	    coords = {
	    	'lat': lat,
	    	'lng': lng
	    }
	}
	if (navigator.geolocation){
		navigator.geolocation.getCurrentPosition(success_function);
	} else {
		alert("Sorry, your browser doesn't support geolocation.");
	}

	return coords;
}
function showUserLocation() {
	if (navigator.geolocation){
		navigator.geolocation.getCurrentPosition(showLocation, errorHandler);
		setStyle("progress", "progress");
	} else {
		alert("Sorry, your browser doesn't support geolocation.");
	}
}

function showLocation(position, zoom = 8) {
	// Create a point
	var pt = new esri.geometry.Point(position.coords.longitude,
									 position.coords.latitude);
	// Create a symbol and pop-up template and add the graphic to the map
	var pic_url = "http://static.arcgis.com/images/Symbols/Shapes/BluePin1LargeB.png";

	if($("#user_profile_picture").length){
		pic_url = $("#user_profile_picture").data('url');
	}

	var symbol = createPictureSymbol(pic_url, 0, 10, 32);
	var attributes = {"lat":pt.y.toFixed(2),"lon":pt.x.toFixed(2)};
	var infoTemplate = new esri.InfoTemplate("My Location","Latitude: ${lat} <br/>Longitude: ${lon}");
	var graphic = new esri.Graphic(pt,symbol,attributes,infoTemplate);
	map.graphics.add(graphic);

	// Position the map
	map.centerAndZoom(pt,zoom);
    setStyle("progress", "progress hidden");
}

function errorHandler(err) {
    setStyle("progress", "progress hidden");
	if(err.code == 1) {
		alert("Error: Access is denied!");
	} else if ( err.code == 2) {
		alert("Error: Position is unavailable!");
	} else 
		alert("Error: " + err);
}

function createPictureSymbol(url, xOffset, yOffset, size) {
    return new esri.symbol.PictureMarkerSymbol(
    {
        "angle": 0,
        "xoffset": xOffset, "yoffset": yOffset, "type": "esriPMS",
        "url": url,
        "contentType": "image/png",
        "width":size, "height": size
    });
}

function clearGeolocationGraphics() {
	map.infoWindow.hide();
	map.graphics.clear();
}

function setStyle(elementName, className) {
	var element = document.getElementById(elementName);
	if (element)
		element.className = className;
}

function autoResize(map) {
	dojo.connect(map, 'onLoad', function (map) {
		dojo.connect(window, 'resize', map, map.resize);
	});
	dojo.connect(map, 'onResize',  function(extent, width, height) {
		map.__resizeCenter = map.extent.getCenter();
		setTimeout(function() {
			map.centerAt(map.__resizeCenter);
		}, 200);
	});
}

function init() {

    var options = {
        basemap: "satellite",
        center: [62.4499, 22.29355],
        zoom: 6,
		infoWindow: new esri.dijit.Popup(null, dojo.create("div"))  // Define a popup
    };
    // Create map
    if($("#mapDiv").length){
		map = new esri.Map("mapDiv",options);
		autoResize(map);
		showUserLocation();
    }

}
dojo.addOnLoad(init);

});

