class WelcomeController < ApplicationController
  def index
    foursquare = OauthFoursquare.new
    @authorization_url = foursquare.authorization_url
    render layout: 'application_lp.html.erb'
  end
end
