require 'oauth2'

class OauthFoursquare
  attr_accessor :client

  def initialize
    @redirect_url = ENV['FSQR_CALLBACK']
    client_id = ENV['FSQR_KEY']
    client_secret = ENV['FSQR_SECRET']

    @client = OAuth2::Client.new client_id, 
                                 client_secret, 
                                :authorize_url => "/oauth2/authorize", 
                                :token_url => "/oauth2/access_token", 
                                :site => "https://foursquare.com/oauth2"

  end
 
  def authorization_url
    @client.auth_code.authorize_url redirect_uri: @redirect_url
  end

  def get_user_token(code)
    token = @client.auth_code.get_token(code, :redirect_uri => @redirect_url)

    token = OAuth2::AccessToken.new(client, token.token, {
      :mode => :query,
      :param_name => "oauth_token",
    })

    token
  end

  def get_user_profile(token)
    uri = "https://api.foursquare.com/v2/users/self"  
    response = token.get(uri)
    response = JSON.parse(response.body)
    
    response = response['response']['user']
    response['token'] = token.token
    response.delete('friends')
      
    response
  end

end
