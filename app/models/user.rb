class User
  include Mongoid::Document
  field :fid, type: String
  field :firstname, type: String
  field :lastname, type: String
  field :homecity, type: String
  field :photo, type: String
  field :contacts, type: Hash
  field :salt, type: String, default: "salt"
  field :token, type: String

  def self.authenticate_with_salt(id, coockie_salt)
    return nil if !id || !coockie_salt
    user = User.find( id )
    (user && user.salt == coockie_salt ) ? user : nil
    rescue => ex
      p "EXCEPTION: #{ex}"
      ex.backtrace.each do |message|
      p "#{message}"
    end
    nil
  end
end
