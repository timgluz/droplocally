class FileItem
  include Mongoid::Document
  include Mongoid::Timestamps
  
  @@s3_bucket = 'droplocally'

  field :s3_name, type: String
  field :original_name, type: String
  field :folder, type: Boolean, default: false
  field :owner_id, type: String
  field :content_type, type: String
  field :private, type: Boolean, default: false
  field :parent_id, type: String
  field :s3_url, type: String

  scope :by_parent_id, ->(fid){ where(parent_id: fid)}

  def self.sanitize_filename(file_name)
    just_filename = File.basename(file_name)
    just_filename.sub(/[\W]+/,'_')
  end

  def owner
    User.where(id: owner_id).shift
  end
  def self.upload_to_s3( fileUp )
    orig_filename =  fileUp.original_filename
    fname = self.sanitize_filename(orig_filename)
    random_val = SecureRandom.hex 10
    filename = "#{random_val}_#{fname}"
    AWS::S3::S3Object.store(filename, 
                            fileUp.read, 
                            'droplocally', 
                            :access => "private")
    filename
  end

  def self.get_s3_url(filename)
    AWS::S3::S3Object.url_for(filename, 
                              'droplocally', 
                              :authenticated => true)
  end

  def self.get_s3_file(filename)
    AWS::S3::S3Object.find(filename, @@s3_bucket)
  end

  def self.delete_project_from_s3(filename)
    AWS::S3::S3Object.delete filename, 'droplocally'
  end
end
