
class FileItemController < ApplicationController
  def upload
    
    file_item = upload_and_store params['Filedata'], params[:fid]
    render json: file_item
  end

  def download

    s3_names = params[:s3_names]
    if s3_names.nil?
      redirect_to :back, alert: "You should select least 1 file"
    end

    s3_names = s3_names.split(",") if s3_names.is_a?(String)
  
    if s3_names.length == 1 
      filename = s3_names.shift
      fileitem = FileItem.where(s3_name: filename).shift
      file_obj = FileItem.get_s3_file(filename)
      send_data file_obj.value, filename: fileitem.original_name, 
                type: file_obj.content_type,
                :disposition => 'attachment'
                
    else
      download_zip s3_names 
    end

  end

  private
    def upload_and_store(file, parent_id)
      @user = current_user
      orig_name = file.original_filename
      s3_name = FileItem.upload_to_s3 file
      s3_url = FileItem.get_s3_url s3_name

      new_file= FileItem.new  s3_name: s3_name,
                              original_name: orig_name,
                              owner_id: @user.id,
                              content_type: file.content_type,
                              private: false,
                              parent_id: parent_id,
                              s3_url: s3_url
      new_file.save
      new_file
    end

    def download_zip(s3_names)
      unless s3_names.blank?
        file_name = "data_export.zip"
        t = Tempfile.new("s3_temp_filename_#{Time.now}")
        Zip::ZipOutputStream.open(t.path) do |z|
          s3_names.each do |s3_name|
            z.put_next_entry(s3_name)

            s3_file = FileItem.get_s3_file(s3_name)
            z.print s3_file.value
          end
        end
        $stdout.binmode

        buf = ''


        send_data t.path, :type => 'application/zip',
                          :disposition => 'attachment',
                          :filename => file_name
                                    
        t.close
        
      end
    end
end
