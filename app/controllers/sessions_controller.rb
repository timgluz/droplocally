
require 'json'

class SessionsController < ApplicationController

  def show
    redirect_to 'welcome#show'
  end 
  def foursquare_callback  
    code = params[:code]
    foursquare = OauthFoursquare.new
    token = foursquare.get_user_token code
    user_profile = foursquare.get_user_profile token
    p user_profile

    user = init_user_model user_profile
    sign_in user
   
    p '---------'
    p user
    p cookies[:user_fid]
    p signed_in?
    p current_user

    if signed_in?
      redirect_to home_url 
      return
    end
    redirect_to root_url, alert: "Authorization failed"
  end

  private 
    def init_user_model(profile)
      user = User.new fid: profile['id'],
                      firstname: profile['firstName'],
                      lastname: profile['lastName'],
                      photo: profile['photo'],
                      homecity: profile['homeCity'],
                      contacts: profile['contact'],
                      token: profile['token']

      user[:contacts][:foursquare] = profile['id']
      user
    end
end
