require 'foursquare2'

class Foursquare
  attr_accessor :client

  def initialize(user)
    @client = Foursquare2::Client.new oauth_token: user.token
  end

  def suggest_venues(q, lat, long)
    @client.suggest_completion_venues ll: "#{lat},#{long}",
                                      query: q,
                                      limit: 10
  end
end
